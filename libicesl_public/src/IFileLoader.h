#pragma once

#include <LibSL/LibSL.h>

#include "IInterface.h"
#include "CSGTree.h"
#include <string>

// {C3F6AEC6-4037-404d-A60E-EC470A41EAD1}
ITF_GUID(Itf_IFileLoader, 
  0xc3f6aec6, 0x4037, 0x404d, 0xa6, 0xe, 0xec, 0x47, 0xa, 0x41, 0xea, 0xd1);

class IFileLoader : public IInterface
{
public:
  //
  // Loads a model file into the scene. 'fname' contains the full path of the file to load.
	// Do not call IPlugEnvironment::getModel as the value at the time IFileLoader::loadFile is called
	// is not representative and subject to change in future releases.
	// loadFile should return a CSGTree instance with at least one top-level brush.
	// It should not contain raw CSGTree instances, only instances of derived classes.
	// If the file is not handled by the plugin, just silently return an empty reference (return DHAutoPtr<CSGTree>());
	// If the file should be handled by the plugin but somehow cannot be loaded, output information in stderr
	// (for example, if there is a parsing error, output a parsing error text, with a line number)
	// then return an empty reference.
	//
  virtual DHAutoPtr<CSGTree> loadFile(const std::string& fname) = 0;
};
