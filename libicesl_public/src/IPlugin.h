#pragma once

#include <LibSL/LibSL.h>

#include "CSGTree.h"
#include "IPluginEnvironment.h"
#include "IFileLoader.h"
#include "IViewRenderer.h"

#include <string>

// {0D0E90C7-26D2-4ae1-8552-205E6338F551}
ITF_GUID(Itf_IPlugin,
  0xd0e90c7, 0x26d2, 0x4ae1, 0x85, 0x52, 0x20, 0x5e, 0x63, 0x38, 0xf5, 0x51);

class IPlugin : public IInterface
{
public:
  // Return true if initialization is successful, false otherwise. If false, IceSL will not subsequently use the plugin. 'env' will never be NULL.
  virtual bool initialize(IPluginEnvironment *env) = 0;

  // Perform some cleanup here, if needed.
  virtual void dispose() = 0;

  virtual IFileLoader *getFileLoader() = 0;

  virtual IViewRenderer *getViewRenderer() = 0;
};

#ifdef WIN32
#define ICESL_PLUGIN_API __declspec(dllexport)
#else
#define ICESL_PLUGIN_API __attribute__((visibility("default")))
#endif

#ifdef WIN32
typedef void(__cdecl * CREATEPLUGINFUNC)(void **);
#else
typedef void(* CREATEPLUGINFUNC)(void **);
#endif
