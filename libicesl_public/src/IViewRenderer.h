#pragma once

#include <LibSL/LibSL.h>

#include "IInterface.h"

// {02C34641-A584-4A93-BD3E-D70516D6ED1F}
ITF_GUID(Itf_IViewRenderer, 
	0x2c34641, 0xa584, 0x4a93, 0xbd, 0x3e, 0xd7, 0x5, 0x16, 0xd6, 0xed, 0x1f);

class IViewRenderer : public IInterface
{
public:
  virtual bool getBrushColor(int brushId, /*out*/ v3f *color) = 0;
};
