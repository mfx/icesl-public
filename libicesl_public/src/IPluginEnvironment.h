
#pragma once

#include <LibSL/LibSL.h>

#include "IInterface.h"
#include "CSGTree.h"
#include <vector>

// {EB4BE774-A86A-43ed-8AD3-D7A3C36A5776}
ITF_GUID(Itf_IPluginEnvironment,
  0xeb4be774, 0xa86a, 0x43ed, 0x8a, 0xd3, 0xd7, 0xa3, 0xc3, 0x6a, 0x57, 0x76);

class IPluginEnvironment : public IInterface
{
public:
  virtual DHAutoPtr<CSGTree> getModel() const = 0;
	
  virtual DHAutoPtr<CSGTree> createBox(float sx, float sy, float sz, bool centered) const = 0;
  virtual DHAutoPtr<CSGTree> createCone(float rb, float rt, float h, bool centered) const = 0;
  virtual DHAutoPtr<CSGTree> createSphere(float radius) const = 0;
  virtual DHAutoPtr<CSGTree> createCylinder(float r, float h, bool centered) const = 0;
  virtual DHAutoPtr<CSGTree> createPolyhedron(const std::vector<v3f>& pos, const std::vector<v3u>& tris) const = 0;
  
  virtual DHAutoPtr<CSGTree> createBrush(DHAutoPtr<CSGTree> node, int id) const = 0;

  virtual DHAutoPtr<CSGTree> createUnion(DHAutoPtr<CSGTree> a, DHAutoPtr<CSGTree> b) const = 0;
  virtual DHAutoPtr<CSGTree> createDifference(DHAutoPtr<CSGTree> a, DHAutoPtr<CSGTree> b) const = 0;
  virtual DHAutoPtr<CSGTree> createIntersection(DHAutoPtr<CSGTree> a, DHAutoPtr<CSGTree> b) const = 0;
  virtual DHAutoPtr<CSGTree> createArrayUnion(const std::vector<DHAutoPtr<CSGTree> >& nodes) const = 0;
  virtual DHAutoPtr<CSGTree> createArrayDifference(const std::vector<DHAutoPtr<CSGTree> >& nodes) const = 0;
  virtual DHAutoPtr<CSGTree> createArrayIntersection(const std::vector<DHAutoPtr<CSGTree> >& nodes) const = 0;
};
