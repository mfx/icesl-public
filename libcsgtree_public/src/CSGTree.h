// --------------------------

#pragma once

#include "IInterface.h"

#include <LibSL/LibSL.h>
#include <vector>

#include "DHAutoPtr.h"

// --------------------------

class /*ICESL_PUBLIC_CLASS*/ CSGTree : public IInterface
{
public:

  using t_Base = CSGTree;

  virtual IInterface *cast(const UniqueID *id) override;

protected:

  static int s_NumInstances;

  static DHAutoPtr<CSGTree> s_EmptyNode;

  m4x4f m_Transform;

  // m_Id uniquely identify instances of CSGTree. This value should never be modified.
  // This is stronger than indentfying by raw pointers because instances allocated after
  // prevously destroyed instances will continue to have different ids, while 
  // the same memory locations might be reused.
  UniqueID   m_Id;

  //CZ 2019-06-19 : this was also defined as operator * in other places
  // there is now a templated operator that does the same thing in AABConversion
  AAB<3> transformAAB(const AAB<3>& inbx, const m4x4f& mat)
  {
    AAB<3> bx;
    v3f m = inbx.minCorner();
    v3f M = inbx.maxCorner();
    v3f p000 = mat.mulPoint(v3f(m[0], m[1], m[2]));
    v3f p001 = mat.mulPoint(v3f(m[0], m[1], M[2]));
    v3f p010 = mat.mulPoint(v3f(m[0], M[1], m[2]));
    v3f p011 = mat.mulPoint(v3f(m[0], M[1], M[2]));
    v3f p100 = mat.mulPoint(v3f(M[0], m[1], m[2]));
    v3f p101 = mat.mulPoint(v3f(M[0], m[1], M[2]));
    v3f p110 = mat.mulPoint(v3f(M[0], M[1], m[2]));
    v3f p111 = mat.mulPoint(v3f(M[0], M[1], M[2]));
    bx.addPoint(p000);
    bx.addPoint(p001);
    bx.addPoint(p010);
    bx.addPoint(p011);
    bx.addPoint(p100);
    bx.addPoint(p101);
    bx.addPoint(p110);
    bx.addPoint(p111);
    return bx;
  }

public:

  CSGTree();
  virtual ~CSGTree() { s_NumInstances--; }

  virtual CSGTree     *clone()              { return new CSGTree(); }
  virtual void         getChildren(std::vector<DHAutoPtr<CSGTree> >& _children) { }
  virtual std::string  type()     const     { return "<empty>"; } // NOTE: beware, this is used to identify empty nodes.
  virtual std::string  toString() const     { return "empty"; }
  virtual AAB<3>       bbox()               { return AAB<3>(); }
  virtual bool         hasToRefresh();      // returns true if a refresh is necessary
  virtual void         doRefresh();         // refreshes immediately, call whenever hasToRefresh returns true
  virtual void         refresh()            { if (hasToRefresh()) { doRefresh(); } }
  virtual bool         hasErrors() const { return false; }
  virtual bool         isSameUnderTransforms(m4x4f m0, m4x4f m1) const;
  virtual bool         isPrimitive() const  { return true; } // NOTE: empty primitive

	virtual uint              getNumChildren() { return 0; }
  virtual DHAutoPtr<CSGTree>& getChild(uint n) { return s_EmptyNode; }
  // virtual void              forgetChildren() { /*leaf implementation*/ }

  /// \brief Required by the deep hierarchy autoptr 
  //         Important: the lambda has to be called on the pointer members held by the class
  virtual void         applyOnChildren(std::function< void(DHAutoPtr<CSGTree> &) >) { }

  // This hash allows to uniquely identify primitives for re-use purposes when scenes
  // are updated. A hash must remain small and unique depending on the content of the
  // primitive. An empty hash string means that the primitive will never be reused (each
  // instance is considered unique). This is the case by default as hashes cannot always
  // be efficiently defined.
  virtual std::string  hash()           { return ""; }

  void   getDescendants(std::vector<DHAutoPtr<CSGTree> >& _descendants);
  void   getDescendantsTransforms(const m4x4f& parent, std::vector<std::pair<DHAutoPtr<CSGTree>, m4x4f> >& _descendants_trsfs);

  void   getPrimitives(std::vector<DHAutoPtr<CSGTree> >& _descendants);

  bool   containsAsDescendant(DHAutoPtr<CSGTree> node);
  bool   containsAsChild(DHAutoPtr<CSGTree> node);

  m4x4f& transform() { return m_Transform; }
  UniqueID id() const  { return m_Id; }

  static int numInstances() { return s_NumInstances; }
};

// --------------------------
