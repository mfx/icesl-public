/*
(c) Inria - Sylvain Lefebvre - 2019-10-02
*/
/*

This implements an auto pointer for deep hierarchies,
allowing stackless recursive destruction of the tree
(and hence preventing stack overflows to occur)

Based on the LibSL Pointer class.

The client class has to implement the following method:
(client class is MyClass)

  /// \brief Important: the lambda has to be called on the pointer members held by the class
  virtual void         applyOnChildren(std::function< void(DHAutoPtr<MyClass>& ) >) { }

In case if inheritance the client base class (the least specialized in the hierarchy() 
has to define the following using:

public:
  using t_Base = BaseClass;

*/

#include <LibSL/LibSL.h>

#include <stack>
#include <set>
#include <functional>

// ------------------------------------------------------

namespace LibSL  {
  namespace Memory {
    namespace Pointer {

// ------------------------------------------------------

/// \brief Implements the LockableRefCounter
class LockableRefCounter
{
private:

  uint m_Counter = 0;
  bool m_Frozen  = false;

public:

  LockableRefCounter(uint value) : m_Counter(value) {}

  void freeze()   { m_Frozen = true; }
  void unfreeze() { m_Frozen = false; }
  bool isFrozen() const { return m_Frozen; }

  uint count() const { return m_Counter; }

  uint increment() { return ++m_Counter; }
  uint decrement() { return --m_Counter; }
};

// ------------------------------------------------------

template <typename T_Type>
class DHAutoPtr;

/// \brief Implements the flat destructor policy
template <typename T_Type> class TransferRefCountFlatDestructor
{
public:

  LockableRefCounter* m_Counter = nullptr;
  T_Type** m_HostRawPointer = nullptr;

  TransferRefCountFlatDestructor() = default;

  void initFrom(T_Type **ptr)
  {
    m_HostRawPointer = ptr;
    if (*ptr != nullptr) {
      // allocate a new counter initialized to 1
      m_Counter = new LockableRefCounter(1);
    } else {
      m_Counter = nullptr;
    }
  }

  void transferFrom(
    T_Type** dst,
    const TransferRefCountFlatDestructor<T_Type>* policy_src,
    T_Type* src)
  {
    m_HostRawPointer = dst;
    *dst = src;
    if (policy_src->m_Counter == nullptr) {
      // src can only be a null pointer, assert
      sl_assert(src == nullptr);
    }
    // point to the common ref counter
    m_Counter = policy_src->m_Counter;
    // if non-null, increment the counter
    if (m_Counter != nullptr) {
      m_Counter->increment();
    }
  }

  template <typename T_Type2>
  void transferCastFrom(
    T_Type** dst,
    const TransferRefCountFlatDestructor<T_Type2>* policy_src,
    T_Type2* src)
  {
    m_HostRawPointer = dst;
    *dst = dynamic_cast<T_Type*>(src);
    if (policy_src->m_Counter == nullptr) {
      // src can only be a null pointer, assert
      sl_assert(src == nullptr);
    }
    // point to the common ref counter
    m_Counter = policy_src->m_Counter;
    // if non-null, increment the counter
    if (m_Counter != nullptr) {
      m_Counter->increment();
    }
  }

  void release(T_Type** ptr)
  {
    m_HostRawPointer = ptr;
    if (m_Counter == nullptr) {
      // ptr can only be null, assert
      sl_assert((*ptr) == nullptr);
    } else if (!m_Counter->isFrozen()) { // if not frozen
      // decrement the counter
      uint count = m_Counter->decrement();
      // did we just release the object?
      if (count == 0) {
        // yes!
#if 1
        // now this gets interesting!
        flatDestructHierarchy(ptr);
#else
        // standard autoptr, for comparison
        delete (m_Counter);
        m_Counter = nullptr;
        delete (*ptr);
        (*ptr) = nullptr;
#endif
      }
    }
  }

  /// \brief Algorithm for flat destruct. Should be in TransferRefCountFlatDestructor, but hitting template hell.
  void flatDestructHierarchy(T_Type** ptr)
  {
    using Base = typename T_Type::t_Base;
    // traverse the hierarchy
    // -> all parents inserted in stack are being deleted
    // -> first is current, since its counter reached zero
    // -> children counts decrease, and if their count reaches zero we recurse
    std::stack< Base * > st;
    std::vector< std::pair<LockableRefCounter**, Base **> > to_destroy;
    std::vector< LockableRefCounter* > to_unfreeze;
    st.push(*ptr);
    while (!st.empty()) {
      Base * current = st.top();
      st.pop();
      // visit children
      current->applyOnChildren([&](DHAutoPtr<Base>& c)
      {
        if (c.m_Counter != nullptr) {
          // freeze
          c.m_Counter->freeze();
          // decrease counter (parent is gone)
          uint new_count = c.m_Counter->decrement();
          // check if we just reached zero, in which case sub-hierarchy must also be destroyed
          if (new_count == 0) {
            // recurse
            st.push(c.raw());
            // flag for destruction
            to_destroy.push_back(std::make_pair(&(c.m_Counter),c.m_HostRawPointer));
            // (keep frozen)
          } else {
            // flag for unfreeze
            to_unfreeze.push_back(c.m_Counter);
          }
        }
      }
      );

    }

    // destroy what got flagged in reverse order (children first)
    for (auto it = to_destroy.rbegin(); it != to_destroy.rend(); it++) {
      delete (*it->first);
      *it->first = nullptr;
      delete (*it->second);
      *it->second = nullptr;
    }

    // delete our host (children are frozen, they will not update their counts)
    delete (m_Counter);
    m_Counter = nullptr;
    delete (*ptr);
    (*ptr) = nullptr;

    // unfreeze (all counts are now up to date)
    for (auto c : to_unfreeze) {
      c->unfreeze();
    }

  }

  uint refCount() const { if (m_Counter == nullptr) return (0); else return m_Counter->count(); }

};

// ------------------------------------------------------

/// \brief Pointer for deep hierarchies

template <typename T_Type>
class DHAutoPtr : public LibSL::Memory::Pointer::Pointer<T_Type, CheckValid, TransferRefCountFlatDestructor >
{
public:

  static int s_InstanceCount;


#ifdef WIN32
  DHAutoPtr() : Pointer() { s_InstanceCount++; }
  explicit DHAutoPtr(const typename Pointer::t_RawPointer &raw) : Pointer(raw) { s_InstanceCount++; }
  DHAutoPtr(const DHAutoPtr &ptr) : Pointer(ptr) { s_InstanceCount++; }
  template <typename T_Type2>
  explicit DHAutoPtr(const DHAutoPtr<T_Type2> &ptr) : Pointer(ptr) { s_InstanceCount++; }
#else
  DHAutoPtr() : Pointer<T_Type, CheckValid, TransferRefCountFlatDestructor>() { s_InstanceCount++; }
  explicit DHAutoPtr(const typename Pointer<T_Type, CheckValid, TransferRefCountFlatDestructor>::t_RawPointer& raw) : Pointer<T_Type, CheckValid, TransferRefCountFlatDestructor>(raw) { s_InstanceCount++; }
  DHAutoPtr(const DHAutoPtr &ptr) : Pointer<T_Type, CheckValid, TransferRefCountFlatDestructor>(ptr) { s_InstanceCount++; }
  template <typename T_Type2>
  explicit DHAutoPtr(const DHAutoPtr<T_Type2> &ptr) : Pointer<T_Type, CheckValid, TransferRefCountFlatDestructor>(ptr) { s_InstanceCount++; }
#endif


  ~DHAutoPtr() { s_InstanceCount--; }

};

template <class T_Type>
int DHAutoPtr<T_Type>::s_InstanceCount = 0;

// ------------------------------------------------------

    } // namespace LibSL::Memory::Pointer
  } // namespace LibSL::Memory
} // namespace LibSL

// ------------------------------------------------------
