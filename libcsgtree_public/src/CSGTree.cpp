// --------------------------

#ifndef WIN32
#include <sys/stat.h>
#include <fcntl.h>
#endif

#include "CSGTree.h"

#include <locale.h>

using namespace std;

// --------------------------

int CSGTree::s_NumInstances = 0;
DHAutoPtr<CSGTree> CSGTree::s_EmptyNode = DHAutoPtr<CSGTree>();

CSGTree::CSGTree()
{
  s_NumInstances++;
  m_Id.generate();
  m_Transform = m4x4f::identity();
}

// --------------------------

IInterface *CSGTree::cast(const UniqueID *id)
{
  if (IsEqualUniqueID(*id, Itf_IInterface))
    return static_cast<IInterface*>(this);
  else
    return NULL;
}

// --------------------------

bool CSGTree::containsAsDescendant(DHAutoPtr<CSGTree> node)
{
  if (node->id() == id()) return true;
	ForIndex(c, getNumChildren()) {
		if (getChild(c)->containsAsDescendant(node)) {
      return true;
    }
  }
  return false;
}

// --------------------------

bool CSGTree::containsAsChild(DHAutoPtr<CSGTree> node)
{
  if (node->id() == id()) return true;
	ForIndex(c, getNumChildren()) {
		if (getChild(c)->id() == node->id()) {
      return true;
    }
  }
  return false;
}

// --------------------------

void CSGTree::getPrimitives(std::vector<DHAutoPtr<CSGTree> >& _primitives)
{
	ForIndex(c, getNumChildren()) {
		if (getChild(c)->isPrimitive()) {
			_primitives.push_back(getChild(c));
		} else {
			getChild(c)->getPrimitives(_primitives);
		}
	}
}

// --------------------------

void CSGTree::getDescendants(std::vector<DHAutoPtr<CSGTree> >& _descendants)
{
	ForIndex(c, getNumChildren()) {
		_descendants.push_back(getChild(c));
	}
	ForIndex(c, getNumChildren()) {
		getChild(c)->getDescendants(_descendants);
	}
}

// --------------------------

void CSGTree::getDescendantsTransforms(const m4x4f& parent,std::vector<std::pair<DHAutoPtr<CSGTree>, m4x4f> >& _descendants_trsfs)
{
	ForIndex(c, getNumChildren()) {
		_descendants_trsfs.push_back(make_pair(getChild(c), parent * transform() * getChild(c)->transform()));
	}
	ForIndex(c, getNumChildren()) {
		getChild(c)->getDescendantsTransforms(parent * transform(), _descendants_trsfs);
	}
}

// --------------------------

bool CSGTree::hasToRefresh()
{
  bool b = false;
	ForIndex(c, getNumChildren()) {
		b = b | getChild(c)->hasToRefresh();
	}
  return b;
}

// --------------------------

void CSGTree::doRefresh()
{
	ForIndex(c, getNumChildren()) {
		getChild(c)->doRefresh();
	}
}

// -----------------------------------------------

bool CSGTree::isSameUnderTransforms(m4x4f m0, m4x4f m1) const
{
  return (m0 == m1);
}

// -----------------------------------------------
