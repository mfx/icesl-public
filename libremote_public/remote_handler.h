// Jean Hergel, Inria, 2017
// SL 2017-05-01
#pragma once

#ifndef EMSCRIPTEN

#include <asio.hpp>

#include <LibSL/LibSL.h>
#include <thread>

class Messaging
{
public:

  struct msg_script_error
  {
    int code;
    int line;
    std::string msg;
  };

  struct msg_update
  {
    int val;
  };

  struct tcp_header
  {
    int buffer_size; // JH warning: this size shouldn't vary at runtime
  };

private:

  std::thread m_ClientThread;
  std::thread m_ServerThread;

  asio::ip::tcp::socket* m_Sock;
  asio::io_service*      m_Service;
  void(*m_ServerCallback)(msg_update& up);
  void(*m_ClientCallback)(msg_script_error& up);

  bool handle_error(int len, asio::error_code& asio_err);

public:

  Messaging()
  {
    m_Service = nullptr;
    m_Sock    = nullptr;
    m_ServerCallback = nullptr;
    m_ClientCallback = nullptr;
  }

  virtual ~Messaging()
  {
    if (m_Sock != nullptr) delete (m_Sock);
    if (m_Service != nullptr) delete (m_Service);
  }

  bool isInitialized() const
  {
    return m_Sock != nullptr;
  }

  // sends a header which contain the size of the next buffer sent
  void send_header(size_t buffsize);
  // processes an expected header
  int process_header(tcp_header& head);

  // main for server (runs from thread)
  static void server()
  {
    std::cerr << "[Messaging::server] started" << std::endl;
    msg_update up;
    while (Messaging::getInstance().process_update(up) > 0) {
      std::cerr << "[Messaging::received (server)] " << up.val << std::endl;
    }
  }

  // main for client (runs from thread)
  static void client()
  {
    std::cerr << "[Messaging::client] started" << std::endl;
    msg_script_error er;
    while (Messaging::getInstance().process_script_error(er) > 0) {
      std::cerr << "[Messaging::received (client) ] " << er.msg << std::endl;
    }
  }

public:

  static Messaging& getInstance()
  {
    static Messaging mes;
    return mes;
  }

  bool initClient(void(*onUpdate)(msg_script_error&));
  void initServer(void(*onUpdate)(msg_update&));

  void send_update(msg_update& up);
  int  process_update(msg_update& up);

  void send_script_error(msg_script_error& err);
  int  process_script_error(msg_script_error& err);

  void spawn_client();
  void end_client();

  void spawn_server();
  void end_server();

};

#else

#include <LibSL/LibSL.h>
#include <string>

class Messaging
{
public:

  struct msg_script_error
  {
    int code;
    int line;
    std::string msg;
  };

  struct msg_update
  {
    int val;
  };

  struct tcp_header
  {
    int buffer_size; // JH warning: this size shouldn't vary at runtime
  };

public:

  Messaging()
  {
  }

  virtual ~Messaging()
  {
  }

  bool isInitialized() const
  {
    return false;
  }

  void send_header(size_t buffsize) {}
  int process_header(tcp_header& head) { return 0; }

  static void server() {}
  static void client() {}

  static Messaging& getInstance()
  {
    static Messaging mes;
    return mes;
  }

  bool initClient(void(*onUpdate)(msg_script_error&)) { return true; }
  void initServer(void(*onUpdate)(msg_update&)) {}

  void send_update(msg_update& up) {}
  int  process_update(msg_update& up) { return 0; }

  void send_script_error(msg_script_error& err) {}
  int  process_script_error(msg_script_error& err) { return 0; }

  void spawn_client() {}
  void end_client() {}

  void spawn_server() {}
  void end_server() {}

};

#endif
