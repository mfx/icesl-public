// Jean Hergel, Inria, 2017

#include "remote_handler.h"

#ifndef EMSCRIPTEN

using asio::ip::tcp;

#include <thread>
#include <LibSL/LibSL.h>

using namespace  std;

#define PORT 50501

// sends a header which contain the size of the next buffer sent.
void Messaging::send_header(size_t buffsize)
{
  tcp_header head;
  head.buffer_size = (int)buffsize;
  m_Sock->send(asio::buffer(&head.buffer_size, sizeof(head.buffer_size)));
}

bool Messaging::handle_error(int len, asio::error_code& asio_err)
{
  if (len == 0) {
    if (asio_err == asio::error::eof) {
      cerr << "disconnected" << endl;
      return true;
    }
    cerr << asio_err.message() << endl;
  }
  return false;
}

// receives an expected header
int Messaging::process_header(tcp_header& head)
{
  asio::error_code asio_err;
  std::vector<int> size(1);
  int len = (int)m_Sock->read_some(asio::buffer(size), asio_err);
  handle_error(len, asio_err);
  if (len < 1) return len;
  head.buffer_size = size[0];
  return len;
}

// init a client
bool Messaging::initClient(void(*onUpdate)(msg_script_error&))
{
  m_Service = new asio::io_service();
  m_Sock = new asio::ip::tcp::socket(*m_Service);
  tcp::endpoint ep(asio::ip::address::from_string("127.0.0.1"), PORT);
  asio::error_code asio_err;
  m_Sock->connect(ep, asio_err);
  bool ok = !handle_error(0, asio_err);
  m_ClientCallback = onUpdate;
  return ok;
}

void Messaging::initServer(void(*onUpdate)(msg_update&))
{
  m_Service = new asio::io_service();
  m_Sock = new asio::ip::tcp::socket(*m_Service);
  tcp::acceptor a(*m_Service, tcp::endpoint(asio::ip::address::from_string("127.0.0.1"), PORT));
  asio::error_code asio_err;
  a.accept(*m_Sock, asio_err);
  handle_error(0, asio_err);
  m_ServerCallback = onUpdate;
}

int Messaging::process_update(msg_update& up)
{
  tcp_header head;
  int len = process_header(head);
  if (len < 1) {
    return len;
  }
  if (head.buffer_size > 512) {
    return -1;
  }
  asio::error_code asio_err;
  std::vector<int> size(1);
  len = (int)m_Sock->read_some(asio::buffer(size), asio_err);
  handle_error(len, asio_err);
  if (len < 1) {
    return len;
  }
  up.val = size[0];
  (*m_ServerCallback)(up);
  return len;
}

void Messaging::send_update(msg_update& up)
{
  try {
    asio::error_code asio_err;
    send_header(sizeof(int));
    m_Sock->send(asio::buffer(&up.val, sizeof(up.val)), asio::socket_base::message_do_not_route, asio_err);
  } catch (std::system_error er) {
    cerr << "socket disconnected" << endl;
  }
}

void Messaging::send_script_error(msg_script_error& err)
{
  try {
    int size = (int)sizeof(char)*(int)err.msg.size();
    send_header(size);
    m_Sock->send(asio::buffer(&err.code, sizeof(err.code)));
    m_Sock->send(asio::buffer(&err.line, sizeof(err.line)));
    std::vector<char> err2char;
    for (int i = 0; i < (int)err.msg.size(); i++) {
      err2char.push_back(err.msg[i]);
    }
    m_Sock->send(asio::buffer(err2char));
  } catch (std::system_error er){
    cerr << "socket disconnected" << endl;
  }
}

int Messaging::process_script_error(msg_script_error& err)
{
  asio::error_code asio_err;
  std::vector<int> size(1);
  tcp_header head;
  int len = process_header(head);
  if (len < 1) {
    return len;
  }
  len = (int)m_Sock->read_some(asio::buffer(size), asio_err);
  handle_error(len, asio_err);
  if (len < 1) {
    return len;
  }
  err.code = size[0];
  len = (int)m_Sock->read_some(asio::buffer(size), asio_err);
  handle_error(len, asio_err);
  if (len < 1) {
    return len;
  }
  err.line = size[0];
  std::vector<char> msg(head.buffer_size);
  len = (int)m_Sock->read_some(asio::buffer(msg), asio_err);
  handle_error(len, asio_err);
  if (len < 1) {
    return len;
  }
  std::string s;
  for (int i = 0; i < head.buffer_size; i++) {
    s.push_back(msg[i]);
  }
  err.msg = s;
  (*m_ClientCallback)(err);
  return len;
}

void Messaging::spawn_client()
{
  m_ClientThread = std::thread(&Messaging::getInstance().client);
}

void Messaging::end_client()
{
  m_ClientThread.join();
}

void Messaging::spawn_server()
{
  m_ServerThread = std::thread(&Messaging::getInstance().server);
}

void Messaging::end_server()
{
  m_ServerThread.join();
}

#endif